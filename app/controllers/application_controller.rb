class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  prepend_view_path Rails.root.join('frontend')

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:firstname, :lastname, :email, :password)}
    devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:firstname, :lastname, :email, :password, :current_password)}
  end
end
